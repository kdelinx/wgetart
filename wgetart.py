#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
# This script writed by Python. And download from Artifactory needed
# for us artifacts. {jar, war} and another branches. For example branch,
# set is ext-release-local. And it has seven folders for our project.
# Full path is - ext-release-local/com/transporttv/gradle/{common/server/etc...} -
# Host: 127.0.0.1
# Port: 8082
# Name: Artifactory OSS
# URI: http://{$HOST}/{$PORT}/artifactory/
# method for download: wget (module)
# request to Artifactory: Web API
'''


import os, shutil
import argparse
import wget
import json
import urllib2

url = 'http://10.8.0.138:8081/artifactory/api/'
release = 'ext-release-local'
snapshot = 'ext-snapshot-local'
stable = 'ext-stable-local'
group_id = '/com/transporttv/gradle/'
path_war = '/opt/ttv-daemon-java/webapps/root.war'
tmp_war = '/tmp/root.war'
cmd_restart = '/opt/ttv-daemon-java/bin/jetty.sh restart'
backup_war = path_war + '.1'


def create_parse():
    parses_arg = argparse.ArgumentParser(
        prog='wgetart',
        usage='%(prog)s [option]',
        description='This script writed by Python. And download from Artifactory needed \
                    for us artifacts. {jar, war} and another branches. For example branch, \
                    set is ext-release-local. And it has seven folders for our project.',
        version='wgetart 0.1 - (C) 2015 Artem Afonin \
                | Released under the GNU GPL',
        epilog='(C) kdelinx 2015. OpenSource, released under the GNU GPL'
    )
    parses_arg.add_argument('-st', '--stable', action='store_const', const=True, help='GET stable version')
    parses_arg.add_argument('-rl', '--release', action='store_const', const=True, help='GET release version')
    parses_arg.add_argument('-sn', '--snapshot', action='store_const', const=True, help='GET snapshot version')
    parses_arg.add_argument('-bu', '--backup', action='store_const', const=True, help='Return previous version')

    return parsesArg


def prev_version():
    os.remove(path_war)
    shutil.move(backup_war, path_war)


def get_list_repo(version, state, build):
    url_str = url + 'storage/' + release + group_id + build + '/' + version + '-' + state
    listing = urllib2.urlopen(url_str)
    json_list = listing.read()
    js_q = json.loads(json_list)
    for i in js_q['children']:
        if 'war' in i['uri']:
            link = url_str + i['uri']
            download_uri = link[0:34] + link[46:]
            os.system("echo Start downloading...\n")
            wget.download(download_uri, tmp_war)
            if not os.path.exists(path_war):
                shutil.move(tmp_war, path_war)
                os.system(cmd_restart)
            else:
                shutil.move(path_war, backup_war)
                shutil.move(tmp_war, path_war)
                os.system(cmd_restart)


if __name__ == "__main__":
    parses_arg = create_parse()
    namespace = parses_arg.parse_args()
    parses_arg.print_help()
    # parses_arg.print_usage()

    if parses_arg.parse_args().stable:
        getListRepo(version='0.2.0', state='STABLE', build='mediaserv')
    elif parses_arg.parse_args().release:
        getListRepo(version='0.2.0', state='RELEASE', build='mediaserv')
    elif parses_arg.parse_args().snapshot:
        getListRepo(version='0.2.0', state='SNAPSHOT', build='mediaserv')
    elif parses_arg.parse_args().backup:
        prev_version()
